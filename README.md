<p align="center">
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  MY PORTFOLIO USING GATSBY + TYPESCRIPT + SANITY + LOCALIZATION I18N
</h1>

## 🚀 Quick start

1.  **Deploy Sanity Graphql**
    > sanity graphql deploy
2.  **Run Sanity dev**
    > sanity dev
3.  **Start Gatsby develop.**
    > gatsby develop
