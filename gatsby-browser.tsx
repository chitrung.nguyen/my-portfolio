/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */

// You can delete this file if you're not using it
import './src/styles/global.scss'

// swiper core styles
import 'swiper/css'
import 'swiper/css/bundle'
import 'swiper/css/effect-cards'

import React from 'react'
import Layout from './src/components/layouts/layout'

export const onClientEntry = () => {
  setTimeout(() => {
    if (document.getElementById('loader-wrapper')) {
      ;(document.getElementById('loader-wrapper') as HTMLElement).style.display = 'flex'
    }
  }, 0)
}

export const onInitialClientRender = () => {
  setTimeout(() => {
    if (document.getElementById('loader-wrapper')) {
      ;(document.getElementById('loader-wrapper') as HTMLElement).style.display = 'flex'
    }
  }, 0)
}

export const onRouteUpdate = () => {
  setTimeout(() => {
    if (document.getElementById('loader-wrapper')) {
      ;(document.getElementById('loader-wrapper') as HTMLElement).style.display = 'none'
    }
  }, 3000)
}

export const wrapPageElement = ({ props, element }) => {
  if (!element.props.children) return null

  return React.cloneElement(
    element, // I18nextProvider
    props,
    React.cloneElement(
      element.props.children, // I18nextContext.Provider
      element.props.children?.props,
      React.createElement(Layout, props, element.props.children?.props.children)
    )
  )
}
