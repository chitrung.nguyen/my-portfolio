/* eslint-disable @typescript-eslint/no-var-requires */
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
})
import type { GatsbyConfig } from 'gatsby'

const config: GatsbyConfig = {
  siteMetadata: {
    title: 'My Portfolio',
    author: 'Trung Nguyen',
    siteUrl: 'https://www.yourdomain.tld',
    social: {
      github: 'https://github.com/NguyenChiTrung1310',
      gitlab: 'https://gitlab.com/chitrung.nguyen',
      linkedin: 'https://www.linkedin.com/in/nguyen-chi-trung-5631761aa',
      gmail: 'nguyenchitrung1310@gmail.com',
      facebook: 'https://www.facebook.com/chitrung.nguyen.5832'
    }
  },
  graphqlTypegen: true,
  plugins: [
    'gatsby-plugin-sass',
    'gatsby-plugin-postcss',
    'gatsby-plugin-image',
    'gatsby-plugin-sitemap',
    'gatsby-plugin-mdx',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-react-svg',
    'gatsby-plugin-portal',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/images/'
      },
      __key: 'images'
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: './src/pages/'
      },
      __key: 'pages'
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'blogs',
        path: './src/pages/blogs'
      },
      __key: 'blogs'
    },
    {
      resolve: 'gatsby-source-sanity',
      options: {
        projectId: process.env.GATSBY_SANITY_PROJECT_ID,
        dataset: process.env.GATSBY_SANITY_PROJECT_DATASET,
        graphqlTag: 'default',
        token: process.env.GATSBY_SANITY_TOKEN
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: './src/locales',
        name: 'locale'
      }
    },
    {
      resolve: 'gatsby-plugin-react-i18next',
      options: {
        localeJsonSourceName: 'locale',
        languages: ['en', 'vi'],
        defaultLanguage: 'en',
        siteUrl: 'http://localhost:8000',
        trailingSlash: 'always',
        i18nextOptions: {
          // debug: true,
          fallbackLng: 'en',
          supportedLngs: ['en', 'vi'],
          defaultNS: 'common'
        }
      }
    },
    {
      resolve: 'gatsby-plugin-alias-imports',
      options: {
        alias: {
          '@lib': 'src/lib',
          '@utils': 'src/utils',
          '@pages': 'src/pages',
          '@hooks': 'src/hooks',
          '@images': 'src/images',
          '@static': 'src/static',
          '@layouts': 'src/layouts',
          '@context': 'src/context',
          '@constants': 'src/constants',
          '@interfaces': 'src/interfaces',
          '@components': 'src/components'
        },
        extensions: ['js', 'tsx', 'ts']
      }
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'gatsby-starter-default',
        short_name: 'starter',
        start_url: '/',
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        // theme_color: `#663399`,
        display: 'minimal-ui',
        icon: 'src/images/buffalo.png' // This path is relative to the root of the site.
      }
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        name: 'authors',
        engine: 'flexsearch',
        engineOptions: {
          tokenize: 'forward'
        },
        query: `
          {
            allSanityArticleBlogs {
              nodes {
                id
                title
                summary
                language
                articleSlug {
                  id
                  slug {
                    current
                  }
                }
                author {
                  id
                  name
                  image {
                    asset {
                      gatsbyImageData(height: 100, width: 100)
                    }
                  }
                }
                categories {
                  id
                  title
                }
                thumbnailImg {
                  asset {
                    gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
                  }
                }
                publishedAt(formatString: "MMM YYYY")
              }
            }
          }
        `,
        ref: 'id',
        index: ['author'],
        store: ['id', 'title', 'summary', 'language', 'author', 'categories', 'articleSlug', 'publishedAt', 'thumbnailImg'],
        normalizer: ({ data }) => data.allSanityArticleBlogs.nodes.map(node => node)
      }
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        name: 'categories',
        engine: 'flexsearch',
        engineOptions: {
          tokenize: 'forward'
        },
        query: `
          {
            allSanityArticleBlogs {
              nodes {
                id
                title
                summary
                language
                articleSlug {
                  id
                  slug {
                    current
                  }
                }
                author {
                  id
                  name
                  image {
                    asset {
                      gatsbyImageData(height: 100, width: 100)
                    }
                  }
                }
                categories {
                  id
                  title
                }
                thumbnailImg {
                  asset {
                    gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
                  }
                }
                publishedAt(formatString: "MMM YYYY")
              }
            }
          }
        `,
        ref: 'id',
        index: ['categories'],
        store: ['id', 'title', 'summary', 'language', 'author', 'categories', 'articleSlug', 'publishedAt', 'thumbnailImg'],
        normalizer: ({ data }) => data.allSanityArticleBlogs.nodes.map(node => node)
      }
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        name: 'blogs',
        engine: 'flexsearch',
        engineOptions: {
          tokenize: 'forward'
        },
        query: `
          {
            allSanityArticleBlogs {
              nodes {
                id
                title
                summary
                language
                articleSlug {
                  id
                  slug {
                    current
                  }
                }
                author {
                  id
                  name
                  image {
                    asset {
                      gatsbyImageData(height: 100, width: 100)
                    }
                  }
                }
                categories {
                  id
                  title
                }
                thumbnailImg {
                  asset {
                    gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
                  }
                }
                publishedAt(formatString: "MMM YYYY")
              }
            }
          }
        `,
        ref: 'id',
        index: ['title', 'summary'],
        store: ['id', 'title', 'summary', 'language', 'author', 'categories', 'articleSlug', 'publishedAt', 'thumbnailImg'],
        normalizer: ({ data }) => data.allSanityArticleBlogs.nodes.map(node => node)
      }
    }
  ]
}

export default config
