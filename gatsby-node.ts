/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')

exports.createPages = ({ graphql, actions: { createPage } }) => {
  return graphql(`
    {
      allSanityArticleBlogs {
        nodes {
          id
          articleSlug {
            slug {
              current
            }
          }
        }
      }
    }
  `).then(result => {
    result.data.allSanityArticleBlogs.nodes.forEach(({ id, articleSlug }) => {
      createPage({
        path: `/blogs/${articleSlug[0].slug.current}`,
        component: path.resolve('./src/pages/blogs/[slug].tsx'),
        context: {
          slug: articleSlug[0].slug.current
        }
      })
    })
  })
}

exports.createPages = ({ graphql, actions: { createPage } }) => {
  return graphql(`
    {
      allSanityProjectsList {
        nodes {
          id
          projectSlug {
            slug {
              current
            }
          }
        }
      }
    }
  `).then(result => {
    result.data.allSanityProjectsList.nodes.forEach(({ id, projectSlug }) => {
      createPage({
        path: `/projects/${projectSlug[0].slug.current}`,
        component: path.resolve('./src/pages/projects/[slug].tsx'),
        context: {
          slug: projectSlug[0].slug.current
        }
      })
    })
  })
}
