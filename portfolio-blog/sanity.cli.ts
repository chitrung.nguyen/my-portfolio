import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'blje1qdt',
    dataset: 'production'
  }
})
