import {defineField, defineType} from 'sanity'

export default defineType({
  title: 'Project Slugs',
  name: 'projectSlugs',
  type: 'document',
  fields: [
    defineField({
      type: 'string',
      name: 'projectName',
      title: 'Project name',
    }),
    defineField({
      type: 'text',
      name: 'description',
      title: 'Description',
    }),
    defineField({
      type: 'slug',
      name: 'slug',
      title: 'Slug',
      options: {
        source: 'projectName',
        maxLength: 96,
      },
    }),
  ],
})
