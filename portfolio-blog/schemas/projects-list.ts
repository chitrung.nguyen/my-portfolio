import {defineField, defineType} from 'sanity'

const articles = {
  title: 'Projects List',
  name: 'projects-list',
  type: 'document',
  fields: [
    defineField({
      type: 'string',
      name: 'language',
      title: 'Select Language',
      options: {
        list: [
          {title: 'English', value: 'en'},
          {title: 'Vietnamese', value: 'vi'},
        ],
      },
    }),
    defineField({
      type: 'array',
      name: 'projectSlug',
      title: 'Project Slug',
      of: [{type: 'reference', to: {type: 'projectSlugs'}}],
    }),
    defineField({
      type: 'string',
      name: 'title',
      title: 'Title',
    }),
    defineField({
      type: 'array',
      name: 'projectType',
      title: 'Project Type',
      of: [{type: 'reference', to: {type: 'projectTypes'}}],
    }),
    defineField({
      type: 'string',
      name: 'teamSize',
      title: 'Team Size',
    }),
    defineField({
      type: 'string',
      name: 'client',
      title: 'Client',
    }),
    defineField({
      type: 'string',
      name: 'technologies',
      title: 'Technologies',
    }),
    defineField({
      type: 'string',
      name: 'functionality',
      title: 'The Functionality',
    }),
    defineField({
      type: 'string',
      name: 'summary',
      title: 'Summary',
    }),
    defineField({
      type: 'image',
      name: 'thumbnailImg',
      title: 'Thumbnail image',
      options: {
        hotspot: true,
      },
    }),
    defineField({
      type: 'image',
      name: 'mainImage',
      title: 'Main image',
      options: {
        hotspot: true,
      },
    }),
  ],
  preview: {
    select: {
      title: 'title',
      author: 'author.name',
      media: 'thumbnailImg',
    },
    prepare(selection: any) {
      const {author} = selection
      return {...selection, subtitle: author && `by ${author}`}
    },
  },
}

export default defineType(articles)
