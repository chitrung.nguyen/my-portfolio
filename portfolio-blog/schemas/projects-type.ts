import {defineField, defineType} from 'sanity'

export default defineType({
  name: 'projectTypes',
  title: 'Project Types',
  type: 'document',
  fields: [
    defineField({
      type: 'string',
      name: 'title',
      title: 'Title',
    }),
    defineField({
      type: 'text',
      name: 'description',
      title: 'Description',
    }),
  ],
})
