import { ChevronRightIcon } from '@heroicons/react/24/solid'
import { Link, useI18next } from 'gatsby-plugin-react-i18next'
import React from 'react'

type Props = {
  prevTo: string
  nextTo?: string
  prevUrlName: string
  nextUrlName: string
}

const Breadcrumb: React.FC<Props> = ({ prevTo, prevUrlName, nextUrlName }) => {
  const { language } = useI18next()
  return (
    <div className='flex-center gap-4'>
      <Link to={prevTo} language={language}>
        <p className='text-darkGrey-700 text-sm'>{prevUrlName}</p>
      </Link>
      <ChevronRightIcon className='w-4' />
      <p className='text-sm font-bold'>{nextUrlName}</p>
    </div>
  )
}

export default Breadcrumb
