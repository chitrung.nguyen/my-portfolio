import { CalendarDaysIcon, XMarkIcon } from '@heroicons/react/24/solid'
import React, { useState } from 'react'
import DatePicker from 'react-date-picker'
import './style.scss'

interface Props {
  onChange?: (val: Date) => void
  labelText?: string
  labelStyle?: string
}

const DatePickerComponent: React.FC<Props> = ({ onChange, labelText, labelStyle }) => {
  const [datepicker, onDatePicker] = useState<Date>(null)

  const onChangeDate = (val: Date) => {
    onDatePicker(val)
    onChange(val)
  }

  return (
    <div>
      {labelText && <label className={[labelStyle, 'font-semibold'].join(' ')}>{labelText}</label>}
      <DatePicker
        clearIcon={<XMarkIcon className='w-5 h-5 text-darkGrey-700' />}
        calendarIcon={<CalendarDaysIcon className='w-5 h-5 text-darkGrey-700' />}
        className='date-picker-custom w-full h-10 bg-white border-none text-darkGrey-primary rounded-full'
        onChange={onChangeDate}
        value={datepicker}
      />
    </div>
  )
}

export default DatePickerComponent
