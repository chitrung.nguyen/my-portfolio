import Button from '@components/@core/Button'
import Portal from '@components/@core/Portal'
import { XMarkIcon } from '@heroicons/react/24/solid'
import { AnimatePresence, motion, Variants } from 'framer-motion'

import React, { ReactNode } from 'react'

interface Props {
  children: ReactNode
  open: boolean
  title?: string
  onClose?: () => void | null
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onSubmit?: any
  modalFooter?: boolean
}

const variants: Variants = {
  closed: {
    opacity: 0,
    scale: [1.05, 1.1, 1.05, 0],
    transition: { duration: 0.4 }
  },
  open: {
    opacity: 1,
    scale: 1,
    transition: {
      duration: 1,
      type: 'spring',
      stiffness: 400,
      damping: 20
    }
  }
}

const Modal: React.FC<Props> = ({ children, open, onClose, title = 'Modal', onSubmit, modalFooter = true }) => {
  const handleClose = () => {
    onClose()
  }

  const header = () => (
    <div className='w-full h-auto flex-between p-5'>
      <h5 className='text-darkGrey'>{title}</h5>
      <XMarkIcon onClick={handleClose} className='w-10 h-10 text-darkGrey cursor-pointer' />
    </div>
  )

  return (
    <AnimatePresence>
      {open && (
        <Portal>
          <motion.div initial='closed' animate='open' exit='closed' className='fixed inset-0 w-full h-full z-[9999] flex items-center justify-center transition-all duration-300'>
            <div className='absolute inset-0 w-full h-full bg-black opacity-50' />
            <motion.div variants={variants} className='min-w-[400px] w-[520px] min-h-[250px] h-auto bg-white rounded-2xl flex flex-col justify-between'>
              {header()}
              <div className='p-5'>{children}</div>
              {modalFooter && (
                <div className='p-5 flex-center gap-2'>
                  {typeof onSubmit === 'function' && (
                    <motion.div className='w-fit group' whileTap={{ scale: 0.9 }} whileHover={{ scale: 1.1 }} transition={{ type: 'spring', stiffness: 400, damping: 8, duration: 0.4 }}>
                      <Button
                        primary
                        onClick={onSubmit}
                        className='text-white capitalize font-bold dark:group-hover:bg-primary group-hover:bg-secondary group-hover:text-white dark:group-hover:border-primary group-hover:border-secondary transition-all duration-300'
                      >
                        Submit
                      </Button>
                    </motion.div>
                  )}
                  <motion.div className='w-fit group' whileTap={{ scale: 0.9 }} whileHover={{ scale: 1.1 }} transition={{ type: 'spring', stiffness: 400, damping: 8, duration: 0.4 }}>
                    <Button
                      primary={typeof onSubmit !== 'function'}
                      onClick={handleClose}
                      className={[
                        typeof onSubmit === 'function'
                          ? 'text-darkGrey border-darkGrey-700'
                          : 'text-white group-hover:text-white dark:group-hover:border-primary group-hover:border-secondary dark:group-hover:bg-primary group-hover:bg-secondary',
                        'capitalize font-bold transition-all duration-300'
                      ]
                        .filter(Boolean)
                        .join(' ')}
                    >
                      Close
                    </Button>
                  </motion.div>
                </div>
              )}
            </motion.div>
          </motion.div>
        </Portal>
      )}
    </AnimatePresence>
  )
}

export default Modal
