import { ReactNode, useEffect } from 'react'
import ReactDOM from 'react-dom'

interface Props {
  children: ReactNode
}

// Use a ternary operator to make sure that the document object is defined

const Portal: React.FC<Props> = ({ children }) => {
  const portalRoot = typeof document !== 'undefined' ? document.getElementById('portal') : null
  const el = typeof document !== 'undefined' ? document.createElement('div') : null

  useEffect(() => {
    portalRoot.appendChild(el)

    return () => {
      portalRoot.removeChild(el)
    }
  }, [])

  if (!el) return null

  return ReactDOM.createPortal(children, el)
}

export default Portal
