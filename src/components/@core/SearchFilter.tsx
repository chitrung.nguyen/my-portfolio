import { ChevronDownIcon } from '@heroicons/react/24/solid'
import useDebounce from '@hooks/useDebounce'
import { useDimensions } from '@hooks/useDimensions'
import { IOption, ISearch } from '@interfaces/index'
import { motion, Variants } from 'framer-motion'
import { useI18next } from 'gatsby-plugin-react-i18next'
import React, { memo, useEffect, useRef, useState } from 'react'

const mainVariants: Variants = {
  open: {
    clipPath: 'inset(0% 0% 0% 0% round 10px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.7,
      delayChildren: 0.3,
      staggerChildren: 0.05
    }
  },
  closed: {
    clipPath: 'inset(10% 50% 90% 50% round 10px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.3,
      delay: 0.3
    }
  }
}

const itemVariants: Variants = {
  open: {
    opacity: 1,
    y: 0,
    transition: { type: 'spring', stiffness: 400, damping: 20 }
  },
  closed: { opacity: 0, y: [-4, -10, 20], transition: { duration: 0.3 } }
}

const toggleSelectVariants: Variants = {
  open: {
    rotateX: 180,
    transition: { duration: 0.3 }
  },
  close: {
    rotateX: 0,
    transition: { duration: 0.3 }
  }
}

interface Props {
  options?: IOption[]
  placeholderSelect?: string
  placeholderInput?: string
  value?: string
  labelStyle?: string
  labelText?: string
  onSearch?: (search: ISearch) => void
  setLoading: React.Dispatch<React.SetStateAction<boolean>>
}

const SearchFilter: React.FC<Props> = ({ placeholderSelect, placeholderInput, value: activeValue = '', options, onSearch, labelStyle, labelText, setLoading }) => {
  const selectRef = useRef()
  const { t } = useI18next()
  const [search, setSearch] = useState<string>('')
  const [toggle, setToggle] = useState<boolean>(false)
  const [optValue, setOptValue] = useState<IOption>({ label: '', value: '', placeholder: '' })
  const dimensions = useDimensions(selectRef)
  const debounce = useDebounce(search, 1000)

  useEffect(() => {
    const findOpt = options.find(opt => activeValue === opt.value)
    if (findOpt) {
      setOptValue(findOpt)
    }
  }, [])

  useEffect(() => {
    const searchFields = {
      option: {
        ...optValue
      },
      search: debounce
    }
    delete searchFields.option.placeholder
    if (typeof onSearch === 'function') onSearch(searchFields)
    setLoading(false)
  }, [debounce])

  const onClickOption = (opt: IOption) => {
    setOptValue(opt)
    const searchFields = {
      option: {
        ...opt
      },
      search
    }
    delete searchFields.option.placeholder
    if (typeof onSearch === 'function') onSearch(searchFields)
    setToggle(false)
  }

  return (
    <motion.div initial={false} animate={toggle ? 'open' : 'closed'} className='w-full relative' ref={selectRef}>
      {labelText && <label className={['text-white uppercase text-sm', labelStyle].join(' ')}>{labelText}</label>}
      <div className='flex-between'>
        <div className='w-3/5 relative'>
          <input
            onChange={e => {
              if (e.target.value) {
                setLoading(true)
                setSearch(e.target.value)
              }
            }}
            placeholder={t(`${optValue.placeholder}`) || t('searchPlaceholder')}
            className='bg-white h-10 text-darkGrey-primary text-sm rounded-l-lg px-4 w-full outline-0 focus:border-solid focus:border dark:focus:border-primary focus:border-secondary placeholder:text-sm placeholder:text-darkGrey-700'
          />
        </div>
        <motion.div className='relative w-2/5'>
          <motion.div
            onClick={() => setToggle(!toggle)}
            className={[
              toggle ? 'dark:border-primary border-secondary' : 'border-l-gray-300',
              'relative border border-solid bg-white h-10 text-darkGrey-primary rounded-r-lg flex-between pl-2 pr-14 cursor-pointer'
            ]
              .filter(Boolean)
              .join(' ')}
          >
            {!activeValue && !optValue.value && <span className='text-darkGrey-700 text-sm'>Select...</span>}
            {optValue.value && <span className='text-darkGrey text-sm'>{optValue.label}</span>}
            <motion.div variants={toggleSelectVariants} className={['cursor-pointer absolute right-2'].join(' ')}>
              <ChevronDownIcon className='w-4 h-4 text-darkGrey-700' />
            </motion.div>
          </motion.div>
        </motion.div>
      </div>
      <motion.div
        variants={mainVariants}
        style={{
          top: dimensions.height || 64,
          left: dimensions.left || 0
        }}
        className={['absolute w-full mt-1 p-2 rounded-lg bg-white shadow-2xl border border-solid border-darkGrey-shade-4 z-[1000]', toggle ? 'pointer-events-auto' : 'pointer-events-none']
          .filter(Boolean)
          .join(' ')}
      >
        {options.map((opt: IOption, index: number) => {
          return (
            <motion.option
              key={opt.value}
              variants={itemVariants}
              onClick={() => onClickOption(opt)}
              className={[
                'px-4 w-full font-normal py-2 tracking-[1.25px] text-left cursor-pointer',
                optValue.value === opt.value || (!optValue.value && index === 0) ? 'bg-secondary dark:bg-primary text-white rounded-lg' : 'text-darkGrey-primary'
              ]
                .filter(Boolean)
                .join(' ')}
            >
              {opt.label}
            </motion.option>
          )
        })}
      </motion.div>
    </motion.div>
  )
}

export default memo(SearchFilter)
