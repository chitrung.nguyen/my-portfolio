import { ChevronDownIcon } from '@heroicons/react/24/solid'
import { IOption } from '@interfaces/index'
import { motion, Variants } from 'framer-motion'
import React, { useEffect, useRef, useState } from 'react'

interface Props {
  options?: IOption[]
  placeholder?: string
  value?: string
  labelStyle?: string
  labelText?: string
  onSelect?: (opt: IOption) => void
}

const mainVariants: Variants = {
  open: {
    clipPath: 'inset(0% 0% 0% 0% round 10px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.7,
      delayChildren: 0.3,
      staggerChildren: 0.05
    }
  },
  closed: {
    clipPath: 'inset(10% 50% 90% 50% round 10px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.3,
      delay: 0.3
    }
  }
}

const itemVariants: Variants = {
  open: {
    opacity: 1,
    y: 0,
    transition: { type: 'spring', stiffness: 400, damping: 20 }
  },
  closed: { opacity: 0, y: [-4, -10, 20], transition: { duration: 0.3 } }
}

const toggleSelectVariants: Variants = {
  open: {
    rotateX: 180,
    transition: { duration: 0.3 }
  },
  close: {
    rotateX: 0,
    transition: { duration: 0.3 }
  }
}

const Select: React.FC<Props> = ({ placeholder, value: activeValue = '', options, onSelect, labelStyle, labelText }) => {
  const selectRef = useRef()
  const [toggle, setToggle] = useState<boolean>(false)
  const [optValue, setOptValue] = useState<IOption>({ label: '', value: '' })

  useEffect(() => {
    const findOpt = options.find(opt => activeValue === opt.value)
    setOptValue(findOpt ? findOpt : { label: '', value: '' })
  }, [activeValue])

  const onClickOption = (opt: IOption) => {
    setOptValue(opt)
    if (typeof onSelect === 'function') onSelect(opt)
    setToggle(false)
  }

  return (
    <motion.div initial={false} animate={toggle ? 'open' : 'closed'} className='relative'>
      {labelText && <label className={[labelStyle, 'font-semibold'].join(' ')}>{labelText}</label>}
      <motion.div
        ref={selectRef}
        onClick={() => setToggle(!toggle)}
        className={[
          toggle ? 'dark:border-primary border-secondary' : 'border-darkGrey-700',
          'relative border border-solid bg-white h-10 text-darkGrey-primary rounded-lg flex-between pl-3 pr-14 cursor-pointer'
        ]
          .filter(Boolean)
          .join(' ')}
      >
        {placeholder && !activeValue && !optValue.value && <span className='text-darkGrey-shade-2 text-sm'>{placeholder}</span>}
        {optValue.value && <span className='text-darkGrey'>{optValue.label}</span>}
        <motion.div variants={toggleSelectVariants} className={['cursor-pointer absolute right-6'].join(' ')}>
          <ChevronDownIcon className='w-4 h-4 text-darkGrey-shade-2' />
        </motion.div>
      </motion.div>
      <div className='drop-shadow-base'>
        <motion.div
          variants={mainVariants}
          className={['w-full absolute top-0 left-0 z-[1000] mt-1 p-2 rounded-lg bg-white', toggle ? 'pointer-events-auto' : 'pointer-events-none'].filter(Boolean).join(' ')}
        >
          {options.map((opt: IOption, index: number) => {
            return (
              <motion.option
                key={opt.value}
                variants={itemVariants}
                onClick={() => onClickOption(opt)}
                className={[
                  'px-4 w-full font-normal py-2 tracking-[1.25px] text-left cursor-pointer',
                  optValue.value === opt.value || (!optValue.value && index === 0) ? 'bg-secondary dark:bg-primary text-white rounded-lg' : 'text-darkGrey-primary'
                ]
                  .filter(Boolean)
                  .join(' ')}
              >
                {opt.label}
              </motion.option>
            )
          })}
        </motion.div>
      </div>
    </motion.div>
  )
}

export default Select
