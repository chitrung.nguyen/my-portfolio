import React from 'react'

interface Props {
  className?: string
  classNameWrapper?: string
  color?: 'darkGrey' | 'black' | 'white' | 'primary'
}

const Spinner: React.FC<Props> = ({ color = 'darkGrey', className, classNameWrapper }) => {
  const spinnerColor = () => {
    switch (color) {
      case 'black':
        return 'spinner-black'
      case 'white':
        return 'spinner-white'
      case 'primary':
        return 'spinner-primary'

      default:
        return 'spinner-darkGrey'
    }
  }

  return (
    <div className={['spinner-wrapper fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2', classNameWrapper].filter(Boolean).join(' ')}>
      <div className={['spinner', spinnerColor(), className].filter(Boolean).join(' ')} />
    </div>
  )
}

export default Spinner
