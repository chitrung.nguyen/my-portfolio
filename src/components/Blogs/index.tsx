import axios from 'axios'
import { graphql, Link, useStaticQuery } from 'gatsby'
import React, { useEffect } from 'react'

import { ArrowLongRightIcon } from '@heroicons/react/24/solid'
import { IBlog } from '@interfaces/index'
import { motion, Variants } from 'framer-motion'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'

interface Props {
  articleBlogs: IBlog[]
  onChangeFetchingData: (data1, data2, data3) => void
}

const variants: Variants = {
  initial: {
    opacity: 0,
    scaleX: 0,
    transformOrigin: 'left'
  },
  hover: {
    opacity: 1,
    scaleX: 1,
    color: '',
    transformOrigin: 'left'
  }
}

const querySearch = graphql`
  {
    localSearchAuthors {
      publicStoreURL
      publicIndexURL
    }
    localSearchCategories {
      publicStoreURL
      publicIndexURL
    }
    localSearchBlogs {
      publicStoreURL
      publicIndexURL
    }
  }
`

const Blogs: React.FC<Props> = ({ articleBlogs, onChangeFetchingData }) => {
  const dataSearch = useStaticQuery(querySearch)

  const { publicStoreURL: blogsPublicStoreURL, publicIndexURL: blogsPublicIndexURL } = dataSearch.localSearchBlogs
  const { publicStoreURL: authorsPublicStoreURL, publicIndexURL: authorsPublicIndexURL } = dataSearch.localSearchAuthors
  const { publicStoreURL: categoriesPublicStoreURL, publicIndexURL: categoriesPublicIndexURL } = dataSearch.localSearchCategories
  useEffect(() => {
    initData()
  }, [])

  const initData = async () => {
    const [{ data: blogsIndex }, { data: blogsStore }, { data: categoriesIndex }, { data: categoriesStore }, { data: authorsIndex }, { data: authorsStore }] = await Promise.all([
      axios.get(`${blogsPublicIndexURL}`),
      axios.get(`${blogsPublicStoreURL}`),
      axios.get(`${categoriesPublicIndexURL}`),
      axios.get(`${categoriesPublicStoreURL}`),
      axios.get(`${authorsPublicIndexURL}`),
      axios.get(`${authorsPublicStoreURL}`)
    ])

    onChangeFetchingData({ blogsIndex, blogsStore }, { categoriesIndex, categoriesStore }, { authorsIndex, authorsStore })
  }

  return (
    <>
      {articleBlogs.map(({ id, thumbnailImg, title, summary, publishedAt, author: { name: authorName, image } = {}, articleSlug }) => (
        <div key={id}>
          <motion.article className='break-inside-avoid h-full w-full relative' initial='initial' animate='initial' whileHover='hover'>
            <Link to={articleSlug[0].slug.current} className='group'>
              <GatsbyImage image={getImage(thumbnailImg.asset)} alt={title} objectFit='cover' objectPosition='center' className='rounded-[32px] shadow-lg' />
              <div className='flex-between pt-4 dark:text-darkGrey-600 text-darkGrey-800'>
                <div className='flex-center gap-2'>
                  <GatsbyImage image={getImage(image.asset)} alt={authorName} objectFit='cover' objectPosition='center' className='rounded-full w-6 h-6' />
                  <p>{authorName}</p>
                </div>
                <p>{publishedAt}</p>
              </div>
              <motion.div className='flex-center justify-start w-fit cursor-pointer gap-2 pt-4'>
                <h5 className='group-hover:text-secondary dark:group-hover:text-primary transition-all duration-300'>{title}</h5>

                <motion.span variants={variants}>
                  <ArrowLongRightIcon className='w-6 h-auto group-hover:text-secondary dark:group-hover:text-primary transition-all duration-300' />
                </motion.span>
              </motion.div>
              <p className='pt-2 group-hover:text-secondary dark:group-hover:text-primary transition-all duration-300'>{summary}</p>
            </Link>
          </motion.article>
        </div>
      ))}
    </>
  )
}

export default Blogs
