import Button from '@components/@core/Button'
import { ThemeContext } from '@context/ThemeContext'
import { MoonIcon, SunIcon } from '@heroicons/react/24/solid'
import { AnimatePresence, motion } from 'framer-motion'
import React, { memo, useContext } from 'react'

const DarkmodeSwitcher: React.FC = () => {
  const [theme, setTheme] = useContext(ThemeContext)

  const toggleDarkMode = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark')
  }
  return (
    <AnimatePresence mode='wait' initial={false}>
      <motion.div style={{ display: 'inline-block' }} key={theme as string} initial={{ y: -20, opacity: 0 }} animate={{ y: 0, opacity: 1 }} exit={{ y: 20, opacity: 0 }} transition={{ duration: 0.2 }}>
        <Button primary onClick={toggleDarkMode} className='h-10 w-10 flex items-center justify-center'>
          {theme === 'dark' ? <MoonIcon className='h-5 w-5 text-white' /> : <SunIcon className='h-5 w-5 text-white-primary' />}
        </Button>
      </motion.div>
    </AnimatePresence>
  )
}

export default memo(DarkmodeSwitcher)
