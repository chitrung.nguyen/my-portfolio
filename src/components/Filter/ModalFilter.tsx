import Button from '@components/@core/Button'
import Input from '@components/@core/Input'
import Select from '@components/@core/Select'
import { IBlog, IOption } from '@interfaces/index'
import { AnimatePresence, motion } from 'framer-motion'
import { useI18next } from 'gatsby-plugin-react-i18next'
import _ from 'lodash'
import React, { memo, useState } from 'react'
import { useFlexSearch } from 'react-use-flexsearch'

interface Props {
  filterOptions?: {
    authors: { id: string; name: string }[]
    categories: { id: string; title: string }[]
    categoriesIndexStore
    authorsIndexStore
    blogsIndexStore
  }
  onChangeFilter?: (filterData: IBlog[], reset: boolean) => void
}

const authorOptions = (arr: { id: string; name: string }[]) => {
  return arr.map(item => ({
    label: item.name,
    value: item.id
  }))
}

const ModalFilterForm: React.FC<Props> = ({ filterOptions, onChangeFilter }) => {
  const [searchFilter, setSearchFilter] = useState<string>('')
  const [activeAuthor, setActiveAuthor] = useState<string>('')
  const [activeCategory, setActiveCategory] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(false)

  const { t, i18n: { language: currentLocale } = {} } = useI18next()

  const { authors, categories, authorsIndexStore, categoriesIndexStore, blogsIndexStore } = filterOptions
  const authorsResult = useFlexSearch(activeAuthor, JSON.stringify(authorsIndexStore.index), authorsIndexStore.store)
  const blogsResult: IBlog[] = useFlexSearch(searchFilter, JSON.stringify(blogsIndexStore.index), blogsIndexStore.store)
  const categoriesResult = useFlexSearch(activeCategory, JSON.stringify(categoriesIndexStore.index), categoriesIndexStore.store)

  const filterDataByLocale = (data: IBlog[]) => {
    return data.length > 0 ? data.filter((item: IBlog) => item.language === currentLocale) : []
  }

  const resultHandler = ({ blogsData, authorsData, categoriesData }: { blogsData: IBlog[]; authorsData: IBlog[]; categoriesData: IBlog[] }) => {
    let mergeData = [...authorsData, ...categoriesData, ...blogsData]
    let reset = false
    if (searchFilter) {
      mergeData = mergeData.filter(data => data.title.toLowerCase().includes(searchFilter))
    }

    if (activeCategory) {
      mergeData = mergeData.filter(data => data.author.id === activeAuthor)
    }

    if (!searchFilter && !activeCategory && !activeAuthor) {
      reset = true
    }

    const filterData = _.uniqBy(mergeData, 'id')
    onChangeFilter(filterData, reset)
  }

  const onSubmit = () => {
    setLoading(true)

    setTimeout(() => {
      const blogsData = filterDataByLocale(blogsResult)
      const authorsData = filterDataByLocale(authorsResult)
      const categoriesData = filterDataByLocale(categoriesResult)
      resultHandler({ blogsData, authorsData, categoriesData })
      setLoading(false)
    }, 1000)
  }

  return (
    <motion.div>
      <AnimatePresence>
        <motion.div>
          <div>
            <Input
              variant='standard'
              label={t('searchLabel')}
              labelStyle='text-darkGrey uppercase text-sm'
              placeholder={t('searchPlaceholder')}
              className='w-full max-w-full min-w-fit'
              classNameInput='w-full max-w-full !h-10 text-darkGrey'
              value={searchFilter}
              onChange={e => setSearchFilter(e.target.value)}
            />
          </div>
          <div className='mt-6'>
            <Select
              onSelect={(opt: IOption) => setActiveAuthor(opt.value)}
              options={authorOptions(authors)}
              labelText={t('authorsLabel')}
              placeholder={t('searchAuthor')}
              labelStyle='text-darkGrey uppercase text-sm'
            />
          </div>
          <div className='mt-6'>
            <label className='uppercase text-sm text-darkGrey font-semibold'>{t('categoryLabel')}</label>
            <div className='mt-1 border border-solid border-darkGrey-700 rounded-lg p-4 flex items-center flex-wrap gap-2'>
              {categories.map(category => (
                <div
                  onClick={() => setActiveCategory(activeCategory === category.id ? '' : category.id)}
                  key={category.id}
                  className={[
                    'w-fit py-1 px-2 rounded-lg border border-solid border-darkGrey text-white bg-darkGrey cursor-pointer text-xs font-semibold',
                    activeCategory === category.id && '!border-primary !bg-primary !text-white'
                  ]
                    .filter(Boolean)
                    .join(' ')}
                >
                  {category.title}
                </div>
              ))}
            </div>
          </div>
          <div className='flex justify-end items-center mt-6'>
            <motion.div className='w-fit group' whileTap={{ scale: 0.9 }} whileHover={{ scale: 1.1 }} transition={{ type: 'spring', stiffness: 400, damping: 8, duration: 0.4 }}>
              <Button
                primary
                loading={loading}
                disabled={loading}
                onClick={onSubmit}
                className='text-white capitalize font-bold dark:group-hover:bg-primary group-hover:bg-secondary group-hover:text-white dark:group-hover:border-primary group-hover:border-secondary transition-all duration-300'
              >
                Submit
              </Button>
            </motion.div>
          </div>
        </motion.div>
      </AnimatePresence>
    </motion.div>
  )
}

export default memo(ModalFilterForm)
