import Button from '@components/@core/Button'
import Input from '@components/@core/Input'
import Select from '@components/@core/Select'
import { IBlog, IOption } from '@interfaces/index'
import { AnimatePresence, motion, Variants } from 'framer-motion'
import { useI18next } from 'gatsby-plugin-react-i18next'
import _ from 'lodash'
import React, { memo, useState } from 'react'
import { useFlexSearch } from 'react-use-flexsearch'

interface Props {
  open?: boolean
  filterOptions?: {
    authors: { id: string; name: string }[]
    categories: { id: string; title: string }[]
    categoriesIndexStore
    authorsIndexStore
    blogsIndexStore
  }
  onChangeFilter?: (filterData: IBlog[], reset: boolean) => void
}

const itemVariants: Variants = {
  open: {
    opacity: 1,
    transition: { type: 'spring', stiffness: 400, damping: 20 }
  },
  closed: {
    opacity: 0,
    transition: { type: 'spring', stiffness: 400, damping: 20 }
  }
}

const authorOptions = (arr: { id: string; name: string }[]) => {
  return arr.map(item => ({
    label: item.name,
    value: item.id
  }))
}

const Filter: React.FC<Props> = ({ open, filterOptions, onChangeFilter }) => {
  const [searchFilter, setSearchFilter] = useState<string>('')
  const [activeAuthor, setActiveAuthor] = useState<string>('')
  const [activeCategory, setActiveCategory] = useState<string>('')

  const [loading, setLoading] = useState<boolean>(false)
  const { t, i18n: { language: currentLocale } = {} } = useI18next()

  const { authors, categories, authorsIndexStore, categoriesIndexStore, blogsIndexStore } = filterOptions
  const authorsResult = useFlexSearch(activeAuthor, JSON.stringify(authorsIndexStore.index), authorsIndexStore.store)
  const blogsResult: IBlog[] = useFlexSearch(searchFilter, JSON.stringify(blogsIndexStore.index), blogsIndexStore.store)
  const categoriesResult = useFlexSearch(activeCategory, JSON.stringify(categoriesIndexStore.index), categoriesIndexStore.store)

  const filterDataByLocale = (data: IBlog[]) => {
    return data.length > 0 ? data.filter((item: IBlog) => item.language === currentLocale) : []
  }

  const resultHandler = ({ blogsData, authorsData, categoriesData }: { blogsData: IBlog[]; authorsData: IBlog[]; categoriesData: IBlog[] }) => {
    let mergeData = [...authorsData, ...categoriesData, ...blogsData]
    let reset = false
    if (searchFilter) {
      mergeData = mergeData.filter(data => data.title.toLowerCase().includes(searchFilter))
    }

    if (activeCategory) {
      mergeData = mergeData.filter(data => data.author.id === activeAuthor)
    }

    if (!searchFilter && !activeCategory && !activeAuthor) {
      reset = true
    }

    const filterData = _.uniqBy(mergeData, 'id')
    resetForm()
    onChangeFilter(filterData, reset)
  }

  const onSubmit = async () => {
    setLoading(true)

    setTimeout(() => {
      const blogsData = filterDataByLocale(blogsResult)
      const authorsData = filterDataByLocale(authorsResult)
      const categoriesData = filterDataByLocale(categoriesResult)
      resultHandler({ blogsData, authorsData, categoriesData })
      setLoading(false)
    }, 1000)
  }

  const resetForm = () => {
    setSearchFilter('')
    setActiveAuthor('')
    setActiveCategory('')
  }

  return (
    <motion.div id='filter-option'>
      <AnimatePresence>
        {open && (
          <motion.div
            initial={{ opacity: 0, scaleX: 0, transformOrigin: 'left', pointerEvents: 'none' }}
            animate={{ opacity: 1, scaleX: 1, transformOrigin: 'left', pointerEvents: 'auto', transition: { delay: 0.3, duration: 0.2 } }}
            exit={{ opacity: 0, scaleX: 0, transformOrigin: 'left', pointerEvents: 'none', transition: { duration: 0.2 } }}
          >
            <motion.div initial={false} animate={open ? 'open' : 'closed'}>
              <motion.div variants={itemVariants}>
                <Input
                  variant='standard'
                  label={t('searchLabel')}
                  labelStyle='dark:text-white text-darkGrey uppercase text-sm'
                  placeholder={t('searchPlaceholder')}
                  className='w-full max-w-full min-w-fit'
                  classNameInput='w-full max-w-full !h-10'
                  value={searchFilter}
                  onChange={e => setSearchFilter(e.target.value)}
                />
              </motion.div>
              <motion.div variants={itemVariants} className='mt-6'>
                <Select
                  value={activeAuthor}
                  onSelect={(opt: IOption) => setActiveAuthor(opt.value)}
                  options={authorOptions(authors)}
                  labelText={t('authorsLabel')}
                  placeholder={t('searchAuthor')}
                  labelStyle='dark:text-white text-darkGrey uppercase text-sm'
                />
              </motion.div>
              <motion.div variants={itemVariants} className='mt-6'>
                <label className='uppercase text-sm text-darkGrey dark:text-white font-semibold'>{t('categoryLabel')}</label>
                <div className='mt-1 border border-solid dark:border-white-primary border-darkGrey-primary rounded-lg p-4 flex items-center flex-wrap gap-2'>
                  {categories.map(category => (
                    <div
                      onClick={() => setActiveCategory(activeCategory === category.id ? '' : category.id)}
                      key={category.id}
                      className={[
                        'w-fit py-1 px-2 rounded-lg border border-solid dark:border-white border-darkGrey dark:text-darkGrey text-white bg-darkGrey dark:bg-white cursor-pointer text-xs font-semibold',
                        activeCategory === category.id && '!border-primary !bg-primary !text-white'
                      ]
                        .filter(Boolean)
                        .join(' ')}
                    >
                      {category.title}
                    </div>
                  ))}
                </div>
              </motion.div>
              <motion.div variants={itemVariants} className='mt-5'>
                <Button onClick={onSubmit} className='w-auto' disabled={loading} loading={loading} primary>
                  Apply
                </Button>
              </motion.div>
            </motion.div>
          </motion.div>
        )}
      </AnimatePresence>
    </motion.div>
  )
}

export default memo(Filter)
