import { graphql, useStaticQuery } from 'gatsby'
import { useI18next } from 'gatsby-plugin-react-i18next'
import React from 'react'
import { Helmet as ReactHelmet } from 'react-helmet'

interface Props {
  title?: string
  description?: string
}

const Helmet: React.FC<Props> = ({ title, description }) => {
  const { t } = useI18next()
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            author
            social {
              github
              gitlab
              linkedin
              gmail
              facebook
            }
          }
        }
      }
    `
  )
  const { siteMetadata } = site
  const metaAuthor = siteMetadata?.author
  const metaTitle = title ? title : siteMetadata?.title
  const metaGithub = siteMetadata?.social.github
  const metaGitlab = siteMetadata?.social.gitlab
  const metaLinkedin = siteMetadata?.social.linkedin
  const metaGmail = siteMetadata?.social.gmail
  const metaFacebook = siteMetadata?.social.facebook
  const metaDescription = description ? description : t('footer.bio_string')

  return (
    <ReactHelmet>
      <title>Portfolio | {metaTitle}</title>
      <meta name='author' content={metaAuthor} />
      <meta name='github' content={metaGithub} />
      <meta name='gitlab' content={metaGitlab} />
      <meta name='linkedin' content={metaLinkedin} />
      <meta name='gmail' content={metaGmail} />
      <meta name='facebook' content={metaFacebook} />
      <meta name='description' content={metaDescription} />
    </ReactHelmet>
  )
}

export default Helmet
