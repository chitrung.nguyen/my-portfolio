import Button from '@components/@core/Button'
import { AnimatePresence, motion } from 'framer-motion'
import { Link, useI18next } from 'gatsby-plugin-react-i18next'
import React, { memo } from 'react'

interface Props {
  isMobile?: boolean
}

const LanguageSwitcher: React.FC<Props> = ({ isMobile }) => {
  const { language, languages, originalPath } = useI18next()

  return (
    <>
      {isMobile ? (
        <ul className='flex-center space-x-3'>
          {languages.map(lng => (
            <li key={lng} className={`relative ${lng === 'vi' ? '' : 'before:absolute before:top-0 before:-right-[6px] before:bg-darkGrey before:block before:w-[1px] before:h-5'}`}>
              <Link to={originalPath} language={lng}>
                <p
                  title={lng}
                  className={`leading-5 tracking-[0.25px] uppercase cursor-pointer relative ${
                    language === lng ? 'text-secondary dark:text-primary font-bold underline leading-[22px] underline-offset-4' : 'font-normal text-darkGrey'
                  }`}
                >
                  {lng}
                </p>
              </Link>
            </li>
          ))}
        </ul>
      ) : (
        <Link to={originalPath} language={languages.filter(item => item !== language).join('')}>
          <AnimatePresence mode='wait' initial={false}>
            <motion.div style={{ display: 'inline-block' }} key={language} initial={{ y: -20, opacity: 0 }} animate={{ y: 0, opacity: 1 }} exit={{ y: 20, opacity: 0 }} transition={{ duration: 0.1 }}>
              <Button className='border-none dark:bg-white-primary bg-darkGrey dark:text-darkGrey text-white-primary font-semibold uppercase p-0 h-10 w-10'>{language}</Button>
            </motion.div>
          </AnimatePresence>
        </Link>
      )}
    </>
  )
}

export default memo(LanguageSwitcher)
