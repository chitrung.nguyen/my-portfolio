import React from 'react'
import './style.scss'

const Loading = () => {
  return <span className='loader' />
}

export default Loading
