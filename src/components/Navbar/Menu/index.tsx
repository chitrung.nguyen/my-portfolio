import Button from '@components/@core/Button'
import { Routes, routes } from '@utils/routes'
import { Link } from 'gatsby'
import { useI18next, useTranslation } from 'gatsby-plugin-react-i18next'
import React, { memo } from 'react'
import './style.scss'

const Menu = () => {
  const { t } = useTranslation()
  const { originalPath, path, defaultLanguage, language } = useI18next()

  const isActive = (url: string) => {
    if (defaultLanguage !== language) {
      return path === `${language}${url}`
    }
    return originalPath === url
  }

  return (
    <div className='w-auto h-full'>
      <ul className='flex h-full items-center gap-8'>
        {routes.map((menu: Routes) => {
          return (
            <li className='relative inline-block transition-all duration-300 menu-effects' key={menu.id}>
              <Button className='border-none p-0 text-darkGrey dark:text-white-primary font-bold'>
                <Link lang={language} className={['py-2 tracking-[2px]', isActive(menu.link) && 'text-secondary dark:text-primary'].filter(Boolean).join(' ')} title={menu.name} to={menu.link}>
                  {t(`navbar.${menu.id}`)}
                </Link>
              </Button>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default memo(Menu)
