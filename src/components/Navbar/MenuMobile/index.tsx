import { Bars3Icon } from '@heroicons/react/24/solid'
import { motion, Variants } from 'framer-motion'
import { Link } from 'gatsby'
import React, { useState } from 'react'

import Button from '@components/@core/Button'
import { Routes, routes } from '@utils/routes'

import LanguageSwitcher from '@components/LanguageSwitcher'
import { useI18next, useTranslation } from 'gatsby-plugin-react-i18next'
import { memo } from 'react'
import './style.scss'

const itemVariants: Variants = {
  open: {
    opacity: 1,
    y: 0,
    transition: { type: 'spring', stiffness: 400, damping: 20 }
  },
  closed: { opacity: 0, y: [-4, -10, 20], transition: { duration: 0.3 } }
}

const mainVariants: Variants = {
  open: {
    clipPath: 'inset(0% 0% 0% 0% round 10px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.7,
      delayChildren: 0.3,
      staggerChildren: 0.05
    }
  },
  closed: {
    clipPath: 'inset(10% 10% 90% 90% round 10px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.3,
      delay: 0.3
    }
  }
}

const MenuMobile: React.FC = () => {
  const { t } = useTranslation()
  const { originalPath } = useI18next()
  const [toggle, setToggle] = useState<boolean>(false)

  return (
    <motion.nav initial={false} animate={toggle ? 'open' : 'closed'} className='relative'>
      <Button onClick={() => setToggle(!toggle)} className='h-10 w-10 flex items-center justify-center dark:border-white-opacity-40 border-darkGrey'>
        <Bars3Icon className='h-5 w-5 dark:text-white-opacity-40 text-darkGrey' />
      </Button>
      <motion.ul
        variants={mainVariants}
        className={['fixed right-5 h-max w-52 top-20 py-2 rounded-lg bg-white shadow-2xl border border-solid border-darkGrey-shade-4 z-[1000]', toggle ? 'pointer-events-auto' : 'pointer-events-none']
          .filter(Boolean)
          .join(' ')}
      >
        {routes.map((menu: Routes) => {
          return (
            <motion.li variants={itemVariants} key={menu.name}>
              <Link className='' title={menu.name} to={menu.link}>
                <Button
                  onClick={() => setToggle(false)}
                  className={['btn-menu w-full border-none font-bold py-4 tracking-[1.25px] text-left', originalPath === menu.link && 'bg-secondary dark:bg-primary text-white']
                    .filter(Boolean)
                    .join(' ')}
                >
                  {t(`navbar.${menu.id}`)}
                </Button>
              </Link>
            </motion.li>
          )
        })}
        <motion.hr variants={itemVariants} />

        <motion.div className='pt-2' variants={itemVariants}>
          <LanguageSwitcher isMobile />
        </motion.div>
      </motion.ul>
    </motion.nav>
  )
}

export default memo(MenuMobile)
