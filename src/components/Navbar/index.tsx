import DarkmodeSwitcher from '@components/DarkmodeSwitcher'
import LanguageSwitcher from '@components/LanguageSwitcher'
import MenuMobile from '@components/Navbar/MenuMobile'
import { ROUTE } from '@constants/routes'
import useScrollView from '@hooks/useScrollView'
import { AnimatePresence, motion } from 'framer-motion'
import { Link } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import React, { memo } from 'react'
import Menu from './Menu'

const Navbar: React.FC = () => {
  const { scrollY, visible } = useScrollView()

  return (
    <motion.div>
      <AnimatePresence>
        {visible && (
          <motion.nav
            initial={{ scaleY: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            animate={{ scaleY: 1, transformOrigin: 'top', pointerEvents: 'auto' }}
            exit={{ scaleY: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            className={['w-full h-20 bg-white-shade-1 dark:layout-gradient fixed top-0 left-0 z-[1000] transition-all duration-75', scrollY > 10 && 'shadow-lg'].filter(Boolean).join(' ')}
          >
            <div className='h-full flex-between container-xxl'>
              <Link className='text-darkGrey dark:text-white-primary flex-center' to={ROUTE.HOME}>
                <StaticImage src='../../images/buffalo.png' alt='Logo' width={40} quality={90} />
                <h4 className='pl-2'>nct.</h4>
              </Link>

              {/* DESKTOP VIEW */}
              <div className='flex-center gap-8 xl:gap-12'>
                <div className='hidden tablet:block'>
                  <Menu />
                </div>

                <div className='flex-center gap-2'>
                  <DarkmodeSwitcher />

                  <div className='hidden tablet:block'>
                    <LanguageSwitcher />
                  </div>

                  {/* MOBILE VIEW */}
                  <div className='block tablet:hidden'>
                    <MenuMobile />
                  </div>
                </div>
              </div>
            </div>
          </motion.nav>
        )}
      </AnimatePresence>
    </motion.div>
  )
}

export default memo(Navbar)
