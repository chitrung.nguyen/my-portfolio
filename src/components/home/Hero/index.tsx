import { ArrowLongDownIcon } from '@heroicons/react/24/solid'
import { motion, MotionProps } from 'framer-motion'
import scrollTo from 'gatsby-plugin-smoothscroll'
import React, { memo } from 'react'

import VoxelDog from '@components/voxel/voxel-dog'
import HeroLightImg from '@images/home/hero-base-light.webp'
import HeroWidgetLightImg from '@images/home/hero-big-widget-light.webp'
import HeroHeartLightImg from '@images/home/hero-heart-widget-light.webp'
import HeroNotifLightImg from '@images/home/hero-notif-widget-light.webp'
import { arrowAnimation, container, onViewAnimation } from '@utils/framer-motion-variants'

const variant = (delay: number, x: number) => {
  return {
    hidden: {
      opacity: 0,
      x: x
    },
    visible: {
      opacity: [0, 0.5, 1],
      x: 0,
      transition: {
        duration: 1,
        ease: 'easeInOut',
        delay
      }
    }
  }
}

const HeroSection: React.FC = () => {
  return (
    <section id='hero-section' className='grid grid-cols-2 tablet_max:grid-cols-1 md:items-center justify-center min-h-screen h-full'>
      {/* LEFT */}
      <div className='sm:-mt-28 md:-mt-36 tablet:mt-0'>
        <h2 className='bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-secondary-blue tracking-[0.25px] md:tracking-[0.44px] xs_max:text-2xl'>I build & develop</h2>
        <h2 className='bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-secondary-blue tracking-[0.25px] md:tracking-[0.44px] xs_max:text-2xl'>applications</h2>
        <motion.div
          onClick={() => scrollTo('#projects-section')}
          initial='hidden'
          animate='visible'
          variants={container}
          whileTap={{ scale: 0.9 }}
          transition={{ type: 'spring', stiffness: 400, damping: 10 }}
          className='inline-block py-3 md:py-4 '
        >
          <h5 className='inline-block tracking-[0.44px] font-normal cursor-pointer'>Explore more projects.</h5>
          <div className='inline-flex'>
            <motion.span variants={onViewAnimation()} {...(arrowAnimation({ y: [-10, 0], scaleY: [0.8, 1.1] }) as MotionProps)}>
              <ArrowLongDownIcon className='w-6 h-6 dark:text-white-primary text-darkGrey inline-block ml-2' />
            </motion.span>
          </div>
        </motion.div>
      </div>
      {/* RIGHT */}
      <div className='relative tablet_max:-order-1 md_max:mt-20'>
        {/* HERO BASE */}
        <motion.div className='absolute -z-10 sm_max:-top-6 top-20 sm:-top-[50px] md:top-0 w-full'>
          <div className='relative aspect-square xs_max:w-full h-auto xs:h-[380px] sm:h-[480px] tablet:h-auto m-auto'>
            <img alt='hero-img' src={HeroLightImg} className='absolute max-h-full h-auto' />
          </div>
        </motion.div>

        {/* HERO HEART */}
        <motion.div initial='hidden' animate='visible' variants={variant(1.6, -10)} className='tablet_max:hidden block absolute top-32 -left-2'>
          <div className='relative aspect-auto m-auto'>
            <img alt='hero-notif-img' src={HeroHeartLightImg} />
          </div>
        </motion.div>

        {/* HERO NOTIFICATION */}
        <motion.div initial='hidden' animate='visible' variants={variant(2.6, 10)} className='tablet_max:hidden block absolute top-14 right-4'>
          <div className='relative aspect-auto m-auto'>
            <img alt='hero-notif-img' src={HeroNotifLightImg} />
          </div>
        </motion.div>

        {/* HERO HEART */}
        <motion.div initial='hidden' animate='visible' variants={variant(3, -10)} className='tablet_max:hidden block absolute top-60 left-4'>
          <div className='relative aspect-auto m-auto'>
            <img alt='hero-notif-img' src={HeroWidgetLightImg} />
          </div>
        </motion.div>

        <VoxelDog />
      </div>
    </section>
  )
}

export default memo(HeroSection)
