import { ArrowLongRightIcon, ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/24/solid'
import { IProjects } from '@interfaces/index'
import { arrowAnimation, container } from '@utils/framer-motion-variants'
import { motion, MotionProps } from 'framer-motion'
import { Link } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import { useTranslation } from 'gatsby-plugin-react-i18next'
import React, { memo, useRef, useState } from 'react'
import Swiper, { Autoplay } from 'swiper'

import { ROUTE } from '@constants/routes'
import { Swiper as ReactSwiper, SwiperSlide } from 'swiper/react'

import './style.scss'

interface Props {
  data: IProjects
}

const ProjectsSection: React.FC<Props> = ({ data: { projectsData } = {} }) => {
  const imgRef = useRef<any>()
  const { t } = useTranslation()
  const [swiper, setSwiper] = useState<Swiper>()
  const [activeIndex, setActiveIndex] = useState<number>(0)
  const [btnChevron, setBtnChevron] = useState<{ activeStart: boolean; activeEnd: boolean }>({
    activeStart: false,
    activeEnd: false
  })

  return (
    <section id='projects-section' className='min-h-screen h-full'>
      <div className='flex items-center justify-between mb-6'>
        <h5 className='uppercase font-normal dark:text-darkGrey-300 text-darkGrey-700'>{t('project-section.section-name')}</h5>
        <div className='flex-1 ml-10 w-full h-auto border-t-[1px] dark:border-darkGrey-300 border-darkGrey-700' />
      </div>
      <div className='w-full h-auto'>
        <div className='grid grid-cols-2 tablet:grid-cols-3 gap-10 tablet:gap-5'>
          <div className='col-span-2'>
            <h5 className='dark:text-darkGrey-300 text-darkGrey-700 font-normal'>{t('project-section.checkout')}</h5>
          </div>
          <div className='col-span-2 tablet:col-span-1'>
            <Link to={ROUTE.PROJECTS}>
              <motion.div initial='hidden' animate='visible' variants={container} whileTap={{ scale: 0.9 }} transition={{ type: 'spring', stiffness: 400, damping: 10 }} className='flex items-center'>
                <h5 className='inline-block tracking-[0.44px] uppercase dark:text-darkGrey-300 text-darkGrey-700'>{t('project-section.all-projects')}</h5>
                <div className='inline-flex'>
                  <motion.span {...(arrowAnimation({ x: [-10, 0], scaleX: [0.8, 1.1] }) as MotionProps)}>
                    <ArrowLongRightIcon className='w-8 h-8 dark:text-darkGrey-300 text-darkGrey-700 inline-block ml-3' />
                  </motion.span>
                </div>
              </motion.div>
            </Link>
          </div>
        </div>
        <div className='relative'>
          <ReactSwiper
            effect='fade'
            slidesPerView={1}
            modules={[Autoplay]}
            grabCursor
            centeredSlides={true}
            spaceBetween={16}
            pagination={{
              clickable: true
            }}
            onSwiper={(s: Swiper) => {
              setSwiper(s)
            }}
            onSlideChange={(s: Swiper) => {
              setActiveIndex(s.activeIndex)
              setBtnChevron({ activeStart: s.activeIndex !== 0, activeEnd: s.isEnd })
            }}
            lazy
            className='swiper-project mt-10 md:mt-8'
            breakpoints={{
              768: {
                slidesPerView: 1.25,
                spaceBetween: 16
              },
              960: {
                slidesPerView: 1.5,
                spaceBetween: 32
              }
            }}
          >
            {projectsData.nodes.map(({ id, thumbnailImg, title, technologies, projectType, projectSlug }, index: number) => (
              <SwiperSlide key={id}>
                <Link to={`projects/${projectSlug[0].slug.current}`}>
                  <motion.div
                    initial={{ scaleY: 0.8 }}
                    animate={{ scaleY: activeIndex === index ? 1 : 0.8, transition: { duration: 0.3 } }}
                    exit={{ scaleY: 0.8, transition: { duration: 0.3 } }}
                    className='w-full h-fit relative rounded-3xl'
                  >
                    <div ref={imgRef}>
                      <GatsbyImage
                        image={getImage(thumbnailImg.asset)}
                        alt={title}
                        objectFit='cover'
                        objectPosition='center'
                        className='rounded-3xl h-full shadow-lg shadow-darkGrey-800 dark:shadow-darkGrey'
                      />
                    </div>
                    {activeIndex === index && (
                      <div className='mt-5'>
                        <div className='flex-between w-full'>
                          <p className='dark:text-darkGrey-600 text-darkGrey-800 font-bold'>
                            <span className='font-normal'>[ {t('project-section.type')} ]: </span>
                            {projectType[0].title}
                          </p>
                          <p className='dark:text-darkGrey-600 text-darkGrey-800 font-bold'>
                            <span className='font-normal'>[ {t('project-section.technologies')} ]: </span>
                            {technologies}
                          </p>
                        </div>
                        <h6 className='mt-4 uppercase'>{title}</h6>
                      </div>
                    )}
                  </motion.div>
                </Link>
              </SwiperSlide>
            ))}
          </ReactSwiper>
          {btnChevron.activeStart && (
            <div
              style={{ top: imgRef?.current?.offsetHeight / 2 - 24 }}
              onClick={() => (swiper as Swiper).slidePrev()}
              className={['flex items-center justify-center h-12 w-12 sm:h-16 sm:w-16 cursor-pointer rounded-full dark:bg-darkGrey-shade-1 bg-darkGrey-600 z-10 absolute -left-4 md:left-2 xl:left-44']
                .filter(Boolean)
                .join(' ')}
            >
              <ChevronLeftIcon className='dark:[&>path]:fill-white-primary [&>path]:fill-darkGrey-primary w-6 h-6 sm:w-8 sm:h-8' />
            </div>
          )}
          {!btnChevron.activeEnd && (
            <div
              style={{ top: imgRef?.current?.offsetHeight / 2 - 24 }}
              onClick={() => (swiper as Swiper).slideNext()}
              className={[
                'flex items-center justify-center h-12 w-12 sm:h-16 sm:w-16 cursor-pointer rounded-full dark:bg-darkGrey-shade-1 bg-darkGrey-600 z-10 absolute -right-4 md:right-2 xl:right-44'
              ]
                .filter(Boolean)
                .join(' ')}
            >
              <ChevronRightIcon className='dark:[&>path]:fill-white-primary [&>path]:fill-darkGrey-primary w-6 h-6 sm:w-8 sm:h-8' />
            </div>
          )}
        </div>
      </div>
    </section>
  )
}

export default memo(ProjectsSection)
