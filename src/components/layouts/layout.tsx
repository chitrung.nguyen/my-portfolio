import Contact from '@components/Contact'
import Footer from '@components/Footer'
import Loading from '@components/Loading'
import Navbar from '@components/Navbar'
import { ThemeProvider as ThemeProviderContext } from '@context/ThemeContext'
import { AnimatePresence, motion } from 'framer-motion'
import { useI18next } from 'gatsby-plugin-react-i18next'

import React from 'react'

interface LayoutProps {
  children: React.ReactNode
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const { path } = useI18next()

  return (
    <ThemeProviderContext>
      <AnimatePresence mode='wait'>
        <div className='relative'>
          <Navbar />
          <div className='h-20 w-full opacity-0 invisible' />
          <motion.main
            key={path}
            initial={{ y: 10, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            exit={{ y: -10, opacity: 0 }}
            transition={{ duration: 0.75 }}
            className='w-full h-auto container-xxl mb-16'
          >
            {children}
          </motion.main>
          <Contact />
          <Footer />
          <div id='loader-wrapper' className='fixed top-0 left-0 w-full h-full bg-darkGrey-primary z-[9999] hidden justify-center items-center'>
            <Loading />
          </div>
        </div>
      </AnimatePresence>
    </ThemeProviderContext>
  )
}

export default Layout
