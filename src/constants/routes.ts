export enum ROUTE {
  HOME = '/',
  PROJECTS = '/projects/',
  ABOUT = '/about/',
  BLOGS = '/blogs/'
}
