import { useEffect, useRef } from 'react'

// Naive implementation - in reality would want to attach
// a window or resize listener. Also use state/layoutEffect instead of ref/effect
// if this is important to know on initial client render.
// It would be safer to  return null for unmeasured states.
export const useDimensions = ref => {
  const dimensions = useRef({ width: 0, height: 0, top: 0, left: 0, right: 0, bottom: 0 })

  useEffect(() => {
    dimensions.current.width = ref?.current?.offsetWidth
    dimensions.current.height = ref?.current?.offsetHeight
    dimensions.current.top = ref?.current?.offsetTop
    dimensions.current.left = ref?.current?.offsetLeft
  }, [])

  return dimensions.current
}
