import { IGatsbyImageData } from 'gatsby-plugin-image'

export interface IBlog {
  id: string
  title: string
  language: string
  articleSlug: {
    id: string
    slug: {
      current: string
    }
  }[]
  categories: {
    id: string
    title: string
  }
  thumbnailImg: {
    asset: IGatsbyImageData
  }
  mainImage: {
    asset: IGatsbyImageData
  }
  author: {
    id: string
    name: string
    image: {
      asset: IGatsbyImageData
    }
  }
  summary: string
  publishedAt: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  body: any
}

export interface IBlogs {
  articleBlogs: {
    nodes: IBlog[]
  }
}

export type IOption = {
  label: string
  value: string
  placeholder?: string
}

export type ISearch = {
  search: string
  option: IOption
}

export interface IProject {
  id: string
  title: string
  projectSlug: {
    id: string
    slug: {
      current: string
    }
  }[]
  projectType: {
    id: string
    title: string
  }
  thumbnailImg: {
    asset: IGatsbyImageData
  }
  mainImage: {
    asset: IGatsbyImageData
  }
  summary: string
  client: string
  functionality: string
  technologies: string
  teamSize: string
}

export interface IProjects {
  projectsData: {
    nodes: IProject[]
  }
}
