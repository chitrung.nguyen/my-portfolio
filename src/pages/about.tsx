import { graphql } from 'gatsby'
import React from 'react'

const AboutPage = () => {
  return <div>AboutPage</div>
}

export default AboutPage

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`
