import Breadcrumb from '@components/@core/Breadcrumb'
import { IBlogs } from '@interfaces/index'
import { PortableText } from '@portabletext/react'
import { graphql, PageProps } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import React from 'react'
import { Helmet } from 'react-helmet'

const DetailBlog = ({ data }: PageProps<IBlogs>) => {
  const articleBlogs = data.articleBlogs.nodes[0]
  const { id, title, body, publishedAt, author: { name: authorName } = {}, mainImage } = articleBlogs

  return (
    <main id={id}>
      <Helmet title={`Blog | ${title}`} />
      <div className='py-3 flex-between pt-10'>
        <Breadcrumb prevTo='/blogs' prevUrlName='Blogs' nextUrlName={title} />

        <p className='text-darkGrey-700 font-semibold text-sm'>
          {authorName}, <span>{publishedAt}</span>
        </p>
      </div>
      <div className='pt-10'>
        <h2>{title}</h2>
        <GatsbyImage image={getImage(mainImage.asset)} alt={title} objectFit='cover' objectPosition='center' className='mt-6 rounded-2xl shadow-lg' />

        <div className='pt-10'>
          <PortableText value={body} />
        </div>
      </div>
    </main>
  )
}

export default DetailBlog

export const query = graphql`
  query ($language: String!, $slug: String) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    articleBlogs: allSanityArticleBlogs(filter: { language: { eq: $language }, articleSlug: { elemMatch: { slug: { current: { eq: $slug } } } } }) {
      nodes {
        id
        title
        author {
          name
        }
        categories {
          id
          title
        }
        mainImage {
          asset {
            gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
          }
        }
        publishedAt(formatString: "MMM DD YYYY")
        body {
          _key
          _type
          style
          children {
            _type
            _key
            marks
            text
          }
        }
      }
    }
  }
`
