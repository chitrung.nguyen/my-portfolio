import Modal from '@components/@core/Modal'
import Blogs from '@components/Blogs'
import Filter from '@components/Filter'
import ModalFilterForm from '@components/Filter/ModalFilter'
import { AdjustmentsVerticalIcon, XMarkIcon } from '@heroicons/react/24/solid'
import useMediaQuery from '@hooks/useMediaQuery'
import useWindowSize from '@hooks/useWindowSize'

import { AnimatePresence, motion, useCycle } from 'framer-motion'
import { graphql, PageProps } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import { useTranslation } from 'gatsby-plugin-react-i18next'
import { IBlog, IBlogs } from 'interfaces'
import React, { useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'

interface Props extends IBlogs {
  categories: {
    nodes: { id: string; title: string }[]
  }
  authors: {
    nodes: { id: string; name: string }[]
  }
}

const blogVariants = (matches: boolean) => {
  return {
    open: {
      marginLeft: 332,
      gap: matches ? 32 : 16,
      transition: {
        transition: { type: 'spring', stiffness: 400, damping: 20 }
      }
    },
    closed: {
      marginLeft: 0,
      gap: matches ? 48 : 24,
      transition: {
        transition: { type: 'spring', stiffness: 400, damping: 20 },
        delay: 0.1
      }
    }
  }
}

const BlogsPage = ({ data }: PageProps<Props>) => {
  const { t } = useTranslation()
  const windowSize = useWindowSize()
  const matches = useMediaQuery('(min-width: 960px)')

  // data store from plugin flexsearch query by graphql
  const [blogsIndexStore, setBlogsIndexStore] = useState(null)
  const [authorsIndexStore, setAuthorsIndexStore] = useState(null)
  const [categoriesIndexStore, setCategoriesIndexStore] = useState(null)

  // blogs data
  const [articleBlogs, setArticleBlogs] = useState<IBlog[]>([])

  // modal
  const [openModal, setOpenModal] = useState<boolean>(false)

  // toggle filter
  const [open, toggleOpen] = useCycle(false, true)

  useEffect(() => {
    setArticleBlogs(data.articleBlogs.nodes || [])
  }, [])

  const categories = data.categories.nodes || []
  const authors = data.authors.nodes || []

  const onChangeFetchingData = ({ blogsIndex, blogsStore }, { categoriesIndex, categoriesStore }, { authorsIndex, authorsStore }) => {
    if (categoriesIndexStore && authorsIndexStore) return
    setBlogsIndexStore({
      index: blogsIndex,
      store: blogsStore
    })
    setCategoriesIndexStore({
      index: categoriesIndex,
      store: categoriesStore
    })
    setAuthorsIndexStore({
      index: authorsIndex,
      store: authorsStore
    })
  }

  return (
    <main id='blogs-page' className='pt-10 tablet:pt-20 w-full h-auto relative'>
      <Helmet title='Blog | Explore blogs' />
      <h3 className='text-center capitalize'>{t('blog-title')}</h3>
      <div
        style={{
          top: windowSize?.height - 100 || 80,
          left: windowSize?.width
        }}
        className='sticky w-fit z-10 block tablet:hidden'
      >
        <motion.div
          animate={{
            scale: [1, 1.2, 1.2, 1, 1],
            rotate: [0, 0, 180, 180, 0],
            borderRadius: ['100%', '100%', '0%', '0%', '100%']
          }}
          transition={{
            duration: 2,
            ease: 'easeInOut',
            times: [0, 0.2, 0.6, 0.8, 1],
            repeat: Infinity,
            repeatDelay: 2
          }}
          className='w-10 h-10 rounded-full bg-primary dark:bg-white-primary relative cursor-pointer'
          onClick={() => setOpenModal(true)}
        >
          <div className='absolute inset-0 w-full h-full bg-primary dark:bg-white blur rounded-full' />
          <AdjustmentsVerticalIcon className='w-5 h-auto [&>path]:fill-white dark:[&>path]:fill-darkGrey absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2' />
        </motion.div>
      </div>
      <AnimatePresence mode='wait' initial={false}>
        <motion.div
          key={open ? 'open' : 'close'}
          initial={{ y: -20, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          exit={{ y: 20, opacity: 0 }}
          transition={{ duration: 0.2 }}
          className={['pt-10 hidden tablet:flex items-center w-fit gap-2 cursor-pointer group', open && 'sticky top-8 z-10'].filter(Boolean).join(' ')}
          onClick={() => toggleOpen()}
        >
          <h6 className='font-normal group-hover:text-secondary dark:group-hover:text-primary transition-all duration-300'>{open ? t('close') : t('filter')}</h6>
          {open ? (
            <XMarkIcon className='w-5 h-auto group-hover:text-secondary dark:group-hover:text-primary transition-all duration-300' />
          ) : (
            <AdjustmentsVerticalIcon className='w-5 h-auto group-hover:text-secondary dark:group-hover:text-primary transition-all duration-300' />
          )}
        </motion.div>
      </AnimatePresence>
      <motion.div initial={false} animate={open ? 'open' : 'closed'} className='relative'>
        <div className='sticky top-[100px] hidden tablet:block'>
          <div className='absolute top-8 w-[300px]'>
            {authorsIndexStore && categoriesIndexStore && blogsIndexStore && (
              <Filter
                open={open}
                onChangeFilter={(filterData: IBlog[], reset: boolean) => setArticleBlogs(reset ? data.articleBlogs.nodes : filterData)}
                filterOptions={{
                  authors,
                  categories,
                  blogsIndexStore,
                  authorsIndexStore,
                  categoriesIndexStore
                }}
              />
            )}
          </div>
        </div>
        <motion.div variants={blogVariants(matches)} className={['', articleBlogs.length > 0 && 'pt-8 columns-1 sm:columns-2 space-y-12'].filter(Boolean).join(' ')}>
          {articleBlogs.length > 0 ? (
            <Blogs onChangeFetchingData={onChangeFetchingData} articleBlogs={articleBlogs} />
          ) : (
            <div className='flex flex-col items-center'>
              <h4 className='inline-block'>The blogs you were searching is not found !</h4>
              <StaticImage width={300} height={300} src='../../images/empty-blog.png' alt='empty-blogs' />
            </div>
          )}
        </motion.div>
      </motion.div>

      {/* Modal */}
      <Modal open={openModal} title='Filter Blog' modalFooter={false} onClose={() => setOpenModal(false)}>
        {authorsIndexStore && categoriesIndexStore && blogsIndexStore && (
          <ModalFilterForm
            onChangeFilter={(filterData: IBlog[], reset: boolean) => {
              setArticleBlogs(reset ? data.articleBlogs.nodes : filterData)
              setOpenModal(false)
            }}
            filterOptions={{
              authors,
              categories,
              blogsIndexStore,
              authorsIndexStore,
              categoriesIndexStore
            }}
          />
        )}
      </Modal>
    </main>
  )
}

export default BlogsPage

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    categories: allSanityCategory {
      nodes {
        id
        title
      }
    }
    authors: allSanityAuthor {
      nodes {
        id
        name
      }
    }
    articleBlogs: allSanityArticleBlogs(filter: { language: { eq: $language } }, sort: { publishedAt: DESC }) {
      nodes {
        id
        title
        summary
        articleSlug {
          id
          slug {
            current
          }
        }
        author {
          id
          name
          image {
            asset {
              gatsbyImageData(height: 100, width: 100)
            }
          }
        }
        categories {
          id
          title
        }
        thumbnailImg {
          asset {
            gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
          }
        }
        publishedAt(formatString: "MMM YYYY")
      }
      totalCount
      pageInfo {
        currentPage
        hasNextPage
        hasPreviousPage
        itemCount
        pageCount
        perPage
        totalCount
      }
    }
  }
`
