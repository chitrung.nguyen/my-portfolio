import { graphql, PageProps } from 'gatsby'
import { useTranslation } from 'gatsby-plugin-react-i18next'
import React from 'react'

import Helmet from '@components/Helmet'

import HeroSection from '@components/home/Hero'
import ProjectsSection from '@components/home/Projects'
import { IProjects } from '@interfaces/index'

const HomePage = ({ data }: PageProps<IProjects>) => {
  const { t } = useTranslation()

  return (
    <main id='instruction-page' className='relative z-10'>
      <Helmet title={t('instruction-page')} />
      <div className='hero-gradient z-0 right-20 sm_max:h-[200px] dark:bg-white-primary bg-darkGrey-600 container-xxl overflow-hidden md_max:pt-10' />

      {/*  HERO SECTION */}
      <HeroSection />

      {/*  PROJECTS SECTION */}
      <ProjectsSection data={data} />

      {/*  ABOUT SECTION */}
      <section id='about-section' className='min-h-screen'>
        <p className='text-white'>ABOUT</p>
      </section>
    </main>
  )
}

export default HomePage

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    projectsData: allSanityProjectsList(filter: { language: { eq: $language } }) {
      nodes {
        id
        projectSlug {
          slug {
            current
          }
          id
        }
        technologies
        title
        projectType {
          id
          title
        }
        thumbnailImg {
          asset {
            gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
          }
        }
      }
    }
  }
`
