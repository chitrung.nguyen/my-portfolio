import { IProjects } from '@interfaces/index'
import { graphql, PageProps } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import React from 'react'
import { EffectCards } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

import './style.scss'

const DetailProject = ({ data }: PageProps<IProjects>) => {
  const projectInfo = data.projectsData.nodes[0]
  const { client, functionality, projectType, summary, teamSize, technologies, title, thumbnailImg, mainImage } = projectInfo

  return (
    <section id='project-detail-page'>
      <div className='w-full h-auto'>
        <div className='grid grid-cols-3 gap-8 mt-10 p-10'>
          <div className='col-span-2'>
            <h2 className='uppercase text-primary dark:text-secondary-blue'>{title}</h2>
          </div>
          <div className='col-span-1'>
            <div className='flex flex-wrap'>
              <div className='w-1/2 pb-20 pr-4'>
                <p className='uppercase dark:text-darkGrey-300 text-darkGrey-700'>Client</p>
                <p>{client}</p>
              </div>
              <div className='w-1/2 pb-20 pr-4'>
                <p className='uppercase dark:text-darkGrey-300 text-darkGrey-700'>Team</p>
                <p>{teamSize}</p>
              </div>
              <div className='w-1/2 pb-20 pr-4'>
                <p className='uppercase dark:text-darkGrey-300 text-darkGrey-700'>Services</p>
                <p>{technologies}</p>
              </div>
              <div className='w-1/2 pb-20 pr-4'>
                <p className='uppercase dark:text-darkGrey-300 text-darkGrey-700'>Type</p>
                <p>{projectType[0].title}</p>
              </div>
            </div>
          </div>
        </div>
        <div className='grid grid-cols-3 gap-8 pt-4'>
          <div className='col-span-1'>
            <p className=''>{summary}</p>
            <p className='pt-8'>
              <span className='text-secondary-blue dark:text-primary font-semibold'>Functionality:</span> {functionality}
            </p>
          </div>
          <div className='-order-1 col-span-2 p-14 pt-0'>
            <Swiper effect='cards' grabCursor modules={[EffectCards]}>
              <SwiperSlide className='bg-transparent rounded-2xl shadow-shadowImg'>
                <GatsbyImage image={getImage(mainImage.asset)} alt={title} objectFit='cover' objectPosition='center' className='rounded-2xl h-[540px]' />
              </SwiperSlide>
              <SwiperSlide className='bg-transparent rounded-2xl shadow-shadowImg'>
                <GatsbyImage image={getImage(thumbnailImg.asset)} alt={title} objectFit='cover' objectPosition='center' className='rounded-2xl h-[540px]' />
              </SwiperSlide>
            </Swiper>
          </div>
        </div>
      </div>
    </section>
  )
}

export default DetailProject

export const query = graphql`
  query ($language: String!, $slug: String) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    projectsData: allSanityProjectsList(filter: { language: { eq: $language }, projectSlug: { elemMatch: { slug: { current: { eq: $slug } } } } }) {
      nodes {
        id
        projectSlug {
          slug {
            current
          }
          id
        }
        summary
        teamSize
        technologies
        title
        functionality
        client
        projectType {
          id
          title
        }
        thumbnailImg {
          asset {
            gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
          }
        }
        mainImage {
          asset {
            gatsbyImageData(layout: CONSTRAINED, sizes: "2000")
          }
        }
      }
    }
  }
`
