import { graphql } from 'gatsby'
import React from 'react'

const ProjectsPage = () => {
  return <div>Projects Page OK</div>
}

export default ProjectsPage

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`
