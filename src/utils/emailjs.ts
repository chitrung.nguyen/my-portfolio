import emailjs from '@emailjs/browser'

export const SendEmail = async (formData: HTMLFormElement) => {
  const serviceID = process.env.GATSBY_EMAILJS_SERVICE_ID
  const templateID = process.env.GATSBY_EMAILJS_TEMPLATE_ID
  const publicKey = process.env.GATSBY_EMAILJS_PUBLIC_KEY

  return await emailjs.sendForm(serviceID, templateID, formData, publicKey)
}
