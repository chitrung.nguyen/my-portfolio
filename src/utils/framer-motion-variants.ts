export const container = {
  visible: {
    transition: {
      staggerChildren: 0.025
    }
  }
}

export const staggerContainer = {
  hidden: {},
  show: {
    transition: {
      staggerChildren: 0.025
    }
  }
}

export const onViewAnimation = (delay = 1) => {
  return {
    hidden: {
      y: '200%',
      transition: { ease: [0.455, 0.03, 0.515, 0.955], duration: 0.85, delay }
    },
    visible: {
      y: 0,
      transition: { ease: [0.455, 0.03, 0.515, 0.955], duration: 0.75, delay }
    }
  }
}

export const arrowAnimation = whileInView => {
  return {
    whileInView,
    transition: {
      repeat: Infinity,
      repeatType: 'mirror',
      duration: 1.5,
      delay: 1
    }
  }
}

export const fadeIn = (direction, type, delay, duration) => ({
  hidden: {
    x: direction === 'left' ? 200 : direction === 'right' ? 200 : 0,
    y: direction === 'up' ? 200 : direction === 'down' ? -200 : 0,
    opacity: 0
  },
  show: {
    x: 0,
    y: 0,
    opacity: 1,
    transition: {
      type,
      delay,
      duration,
      ease: 'easeOut'
    }
  }
})

export const zoomIn = (delay, duration) => ({
  hidden: {
    scale: 0,
    opacity: 0
  },
  show: {
    scale: 1,
    opacity: 1,
    transition: {
      type: 'tween',
      delay,
      duration,
      ease: 'easeOut'
    }
  }
})
