import { ROUTE } from '@constants/routes'

export type Routes = {
  link: string
  name: string
  id?: string
}

export const routes: Routes[] = [
  {
    link: ROUTE.HOME,
    name: 'Introduction',
    id: 'introduction'
  },
  {
    link: ROUTE.ABOUT,
    name: 'About',
    id: 'about'
  },
  {
    link: ROUTE.PROJECTS,
    name: 'Projects',
    id: 'projects'
  },
  {
    link: ROUTE.BLOGS,
    name: 'Blogs',
    id: 'blogs'
  }
]
