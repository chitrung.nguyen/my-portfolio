/* eslint-disable @typescript-eslint/no-var-requires */
/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin')
module.exports = {
  content: ['./src/pages/**/*.{js,jsx,ts,tsx}', './src/components/**/*.{js,jsx,ts,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      keyframes: {
        fill: {
          '0%': { width: '0%', height: '1px' },
          '50%': { width: '100%', height: '1px' },
          '100%': { width: '100%', height: '100%', backgroundColor: '#1A232E' }
        },
        fillDark: {
          '0%': { width: '0%', height: '1px' },
          '50%': { width: '100%', height: '1px' },
          '100%': { width: '100%', height: '100%', backgroundColor: 'rgba(255, 255, 255, 0.8)' }
        }
      },
      animation: {
        fill: 'fill 0.8s forwards',
        fillDark: 'fillDark 0.8s forwards'
      },
      container: {
        center: true,
        padding: {
          DEFAULT: '1.25rem',
          md: '2.5rem',
          tablet: '5rem'
        }
      },
      screens: {
        xs: '528px',
        xs_max: { max: '527px' },

        sm: '640px',
        sm_max: { max: '639px' },

        md_max: { max: '767px' }, // (md: min-width = 768px)

        tablet: '960px',
        tablet_max: { max: '959px' },

        lg: '1140px',
        lg_max: { max: '1139px' },

        xl: '1320px',
        xl_max: { max: '1319px' },

        '2xl': '1440px'
      },
      fontSize: {
        xs: [
          '12px',
          {
            lineHeight: '18px'
          }
        ],
        sm: [
          '14px',
          {
            lineHeight: '22px'
          }
        ],
        base: [
          '16px',
          {
            lineHeight: '24px'
          }
        ],
        lg: [
          // <h6></h6>
          '20px',
          {
            lineHeight: '28px'
          }
        ],
        xl: [
          // <h5></h5>
          '24px',
          {
            lineHeight: '29px'
          }
        ],
        '2xl': [
          // <h4></h4>
          '34px',
          {
            lineHeight: '41px'
          }
        ],
        '3xl': [
          // <h3></h3>
          '48px',
          {
            lineHeight: '58px'
          }
        ],
        '4xl': [
          // <h2></h2>
          '60px',
          {
            lineHeight: '73px'
          }
        ],
        '5xl': [
          // <h1></h1>
          '96px',
          {
            lineHeight: '116px'
          }
        ]
      },
      lineHeight: {
        '5xl': '116px',
        '4xl': '73px',
        '3xl': '58px',
        '2xl': '41px',
        xl: '29px',
        lg: '28px',
        base: '24px',
        sm: '22px',
        xs: '18px'
      },
      fontWeight: {
        medium: 500,
        semibold: 600,
        bold: 700,
        800: 800
      },
      fontStyle: {
        normal: 'normal',
        italic: 'italic'
      },
      colors: {
        primary: {
          DEFAULT: '#F87171'
        },
        secondary: {
          DEFAULT: '#805ad5',
          blue: '#0EA5E9'
        },
        darkGrey: {
          DEFAULT: '#2D2B2C',
          primary: '#1A232E',
          // SHADES
          'shade-1': 'rgba(228, 228, 228, 0.5)',
          'shade-2': '#808191',
          'shade-3': 'rgba(228, 228, 228, 0.25)',
          // OPACITIES
          'opacity-70': 'rgba(45, 43, 44, 0.7)',
          100: '#F1F1F1',
          200: '#EBF0F0',
          300: 'rgba(235, 240, 240, 0.5)', // #EBF0F0, 70%
          400: 'rgba(235, 240, 240, 0.1);', // #EBF0F0, 10%
          500: '#E8E8E8',
          600: 'rgba(207, 219, 213, 0.6)',
          700: '#949494',
          800: '#6B6B6B'
        },
        white: {
          DEFAULT: '#FFFFFF',
          primary: 'rgba(255, 255, 255, 0.8)',
          // SHADES
          'shade-1': 'rgb(237, 237, 237)',
          // OPACITIES
          'opacity-40': 'rgba(255, 255, 255, 0.6)',
          'opacity-30': 'rgba(255, 255, 255, 0.3)'
        }
      },
      backgroundImage: {
        'gradient-radial-darkGrey': 'radial-gradient(farthest-side,#2D2B2C 92%,#0000);',
        'gradient-radial-white': 'radial-gradient(farthest-side,#FFFFFF 92%,#0000);',
        'gradient-radial-black': 'radial-gradient(farthest-side,#000000 92%,#0000);',
        'gradient-radial-primary': 'radial-gradient(farthest-side,#F48026 92%,#0000);'
      },
      dropShadow: {
        base: ['0 -2px 4px rgb(0 0 0 / 0.1)', '0 1px 2px rgb(0 0 0 / 0.1)']
        // 'drop-shadow(0 -2px 4px rgb(0 0 0 / 0.1)) drop-shadow(0 1px 2px rgb(0 0 0 / 0.1))'
      },
      boxShadow: {
        shadowImg: '0px 0px 24px -6px rgba(45,43,44,0.75)'
      }
    }
  },
  plugins: [
    plugin(function ({ addBase, theme, addComponents }) {
      addComponents({
        '.flex-between': {
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        },
        '.flex-center': {
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        },
        '.container-xl': {
          maxWidth: '100%',
          width: '100%',
          margin: 'auto',
          '@screen xl': {
            maxWidth: '1280px',
            width: '100%'
          }
        },
        '.container-xxl': {
          maxWidth: '100%',
          width: '100%',
          margin: 'auto',
          padding: '0 20px',
          '@screen 2xl': {
            maxWidth: '1440px',
            width: '100%'
          },
          '@screen lg': {
            maxWidth: '1280px',
            width: '100%'
          }
        },
        '.layout-gradient': {
          background: 'linear-gradient(90deg, #28334C 0%, #4F616B 80%, #284767 100%)'
          // background: 'linear-gradient(90deg, #353E56 0%, #4F616B 80%, #284767 100%)'
          // background: 'linear-gradient(90deg, #252E45 0%, #4F616B 80%, #353E56 100%)'
        },
        '.footer-gradient': {
          background: 'linear-gradient(90deg, #252E45 0%, #4F616B 80%, #353E56 100%)'
        },
        '.hero-gradient': {
          position: 'absolute',
          width: '200px',
          height: '438px',
          top: '0px',
          right: '0px',

          background: 'rgba(108, 99, 255, 0.75)',
          filter: 'blur(190px)'
        },
        '.bg-gradient': {
          position: 'absolute',
          width: '304px',
          height: '300px',
          left: '20%',
          top: '0',

          background: 'rgba(45, 72, 152, 0.75)',
          filter: 'blur(200px)',
          transform: 'rotate(-53.13deg)'
        },
        '.bg-gradient-2': {
          position: 'absolute',
          width: '304px',
          height: '300px',
          left: '20%',
          top: '0',
          background: 'linear-gradient( 97.86deg, #a509ff 0%,#34acc7 53.65%,#a134c7 100%)',
          filter: 'blur(200px)',
          transform: 'rotate(-53.13deg)'
        },
        '.bg-contact': {
          backgroundImage: 'linear-gradient(60deg, rgba(128, 90, 213, 0.8) 14%, rgba(40, 51, 76, 0.9), rgba(40, 51, 76, 1), rgba(248, 113, 113, 0.8) 100%), url("/images/contact.jpg")',
          backgroundPosition: 'center',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat'
        }
      })
      addBase({
        h1: {
          fontSize: theme('fontSize.5xl'),
          lineHeight: theme('lineHeight.5xl'),
          fontStyle: theme('fontStyle.normal'),
          fontWeight: theme('fontWeight.bold'),
          letterSpacing: '-1.5px'
        },
        h2: {
          fontSize: theme('fontSize.4xl'),
          lineHeight: theme('lineHeight.4xl'),
          fontStyle: theme('fontStyle.normal'),
          fontWeight: theme('fontWeight.bold'),
          '@screen tablet_max': {
            fontSize: theme('fontSize.3xl'),
            lineHeight: theme('lineHeight.3xl'),
            fontStyle: theme('fontStyle.normal'),
            fontWeight: theme('fontWeight.bold')
          }
        },
        h3: {
          fontSize: theme('fontSize.3xl'),
          lineHeight: theme('lineHeight.3xl'),
          fontStyle: theme('fontStyle.normal'),
          fontWeight: theme('fontWeight.bold'),
          '@screen tablet_max': {
            fontSize: theme('fontSize.2xl'),
            lineHeight: theme('lineHeight.2xl'),
            fontStyle: theme('fontStyle.normal'),
            fontWeight: theme('fontWeight.bold')
          }
        },
        h4: {
          fontSize: theme('fontSize.2xl'),
          lineHeight: theme('lineHeight.2xl'),
          fontStyle: theme('fontStyle.normal'),
          fontWeight: theme('fontWeight.bold'),
          letterSpacing: '0.25px'
        },
        h5: {
          fontSize: theme('fontSize.xl'),
          lineHeight: theme('lineHeight.xl'),
          fontStyle: theme('fontStyle.normal'),
          fontWeight: theme('fontWeight.bold'),
          '@screen tablet_max': {
            fontSize: theme('fontSize.lg'),
            lineHeight: theme('lineHeight.lg'),
            fontStyle: theme('fontStyle.normal'),
            fontWeight: theme('fontWeight.bold')
          }
        },
        h6: {
          fontSize: theme('fontSize.lg'),
          lineHeight: theme('lineHeight.lg'),
          fontStyle: theme('fontStyle.normal'),
          fontWeight: theme('fontWeight.bold'),
          letterSpacing: '0.15px',
          '@screen tablet_max': {
            fontSize: theme('fontSize.base'),
            lineHeight: theme('lineHeight.base'),
            fontStyle: theme('fontStyle.normal'),
            fontWeight: theme('fontWeight.bold')
          }
        },
        p: {
          fontSize: theme('fontSize.base'),
          lineHeight: theme('lineHeight.base'),
          fontStyle: theme('fontStyle.normal'),
          letterSpacing: '0.44px',
          '@screen tablet_max': {
            letterSpacing: '0.25px',
            fontSize: theme('fontSize.sm'),
            lineHeight: theme('lineHeight.sm')
          }
        }
      })
    })
  ]
}
